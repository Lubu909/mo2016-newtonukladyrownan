import java.util.Scanner;

public class Uklad {
	private double x;
	private double y;
	private double xPrev;
	private double yPrev;
	private int maksymalna;
	private double przyblizenie;
	Scanner reader = new Scanner(System.in);
	
	public Uklad(){
		setter();
		do{
			System.out.print("Podaj przyblizenie: ");
			przyblizenie = reader.nextDouble();
			if((przyblizenie<=0)||(przyblizenie>=1)) System.out.println("Przyblizenie musi byc wieksze od 0 i mniejsze od 1!");
		}while((przyblizenie<=0)||(przyblizenie>=1));
		maksymalna = 20;
		/*
		do{
			System.out.print("Podaj maksymalna liczbe iteracji: ");
			maksymalna = reader.nextInt();
			if(maksymalna<=0) System.out.println("Maksymalna liczba iteracji musi byc wieksza lub rowna 0!");
		}while(maksymalna<0);
		*/
	}
	
	private void setter(){
		System.out.print("Podaj x0: ");
		x = reader.nextDouble();
		System.out.print("Podaj y0: ");
		y = reader.nextDouble();
	}
	
	private double[] f(){
		double wynik[] = new double[2];
		wynik[0]= 4 * Math.pow(x, 2) + Math.pow(y, 2) - 4;
		wynik[1]= x - Math.pow(y, 2) + 2 * y - 1; 
		return wynik;
	}
	
	private double[][] fPrim(){
		double wynik[][] = new double[2][2];
		wynik[0][0] = 8 * x;
		wynik[0][1] = 2 * y;
		wynik[1][0] = 1;
		wynik[1][1] = -2 * y + 2;
		return wynik;
	}
	
	private Boolean odwracalna(double macierz[][]){
		Boolean odp;
		double wyznacznik = macierz[0][0]*macierz[1][1] - macierz[0][1]*macierz[1][0];
		if(wyznacznik==0) odp = false;
		else odp = true;
		return odp;
	}
	
	private double[][] odwracanieMacierzy(double macierz[][]){
		double wynik[][] = new double[2][2];
		double wyznacznik = macierz[0][0]*macierz[1][1] - macierz[0][1]*macierz[1][0];
		wynik[0][0] = macierz[1][1] * 1/wyznacznik;
		wynik[0][1] = - macierz[0][1] * 1/wyznacznik;
		wynik[1][0] = - macierz[1][0] * 1/wyznacznik;
		wynik[1][1] = macierz[0][0] * 1/wyznacznik;
		return wynik;
	}
	
	private double[] mnozenieMacierzy(double prim[][], double F[]){
		double wynik[] = new double[2];
		wynik[0] = prim[0][0]*F[0] + prim[0][1]*F[1];
		wynik[1] = prim[1][0]*F[0] + prim[1][1]*F[1];
		return wynik;
	}
	
	private double[] odejmowanieMacierzy(double lewa[], double prawa[]){
		double wynik[] = new double[2];
		wynik[0] = lewa[0] - prawa[0];
		wynik[1] = lewa[1] - prawa[1];
		return wynik;
	}
	
	public void licz(){
		int ilosc = 0;
		do{
			ilosc++;
			double[][] prim = fPrim();
			if(odwracalna(prim)){
				double next[];
				double dane[] = new double[2];
				dane[0] = x;
				dane[1] = y;
				double[] f = f();
				next = odejmowanieMacierzy(dane,mnozenieMacierzy(odwracanieMacierzy(prim),f));
				xPrev = x;
				yPrev = y;
				x = next[0];
				y = next[1];
				//System.out.println("x" + (ilosc-1) + " = " + xPrev + "\ty" + (ilosc-1) + " = " + yPrev);					//pomocnicze wypisywanie
			}
			else{
				System.out.println("Uzyskana macierz jest nieodwracalna, podaj inne wartosci x0,y0");
				setter();
				licz();
				return;
			}
		}while((ilosc<maksymalna)&&((Math.abs(x-xPrev)>przyblizenie)&&(Math.abs(y-yPrev)>przyblizenie)));
		//System.out.println("x" + ilosc + " = " + x + "\ty" + ilosc + " = " + y);											//pomocnicze wypisywanie
		//System.out.println("x = " + (x-(x%przyblizenie)) + "\ty = " + (y-(y%przyblizenie)) + "\t+/- " + przyblizenie);	//wersja z przyciananiem
		System.out.println("x = " + x + "\ty = " + y + "\t+/- " + przyblizenie);
		System.out.println("Ilosc iteracji: " + ilosc);
		System.out.println();
	}
	
	public static void main(String args[]){
		String opcja;
		Scanner reader = new Scanner(System.in);
		do{
			Uklad dane = new Uklad();
			dane.licz();
			do{
				System.out.print("Czy chcesz przetestowac metode dla kolejnych danych?[t/n] ");
				opcja = reader.nextLine();
				opcja.trim();
				opcja.toLowerCase();
			}while(!opcja.matches("^(?:t|n)$"));
		}while(opcja.equals("t"));
		reader.close();
	}
	
}
